#include <iostream>


void EvenOdd(int n, int y) {
    for (int i = y; i <= n; i += 2) {
        std::cout << i << " ";
    }
    std::cout << '\n';
}

int main() {

    int N = 10;
    // first task - print even numbers from 0 to N
    std::cout << "First Task - Print Even Numbers: " << '\n';
    for (int i = 0; i <= N; i++) {
        if (i % 2 == 0) {
            std::cout << i << " ";
        }
    }
    std::cout << '\n' << "-------------" << '\n';

    // second task - depending on 0 or 1, print even/odd numbers
    int choice;
    std::cout << "Second Task - Depending on additional parametr print even or odd numbers. " << '\n';
    std::cout << "Please, enter 0 for Even numbers and 1 for Odd: ";
    std::cin >> choice;
    while (choice < 0 || choice > 1) {
        std::cout << "Please, enter the correct value (0 or 1): ";
        std::cin >> choice;
    }
    EvenOdd(N, choice);
    
    return 0;
}
